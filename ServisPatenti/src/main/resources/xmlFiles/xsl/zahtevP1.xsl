<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:z="http://www.xml22.rs/zahtev-p1/"
                xmlns:slo="http://www.xml22.rs/slozeni/" version="2.0">
    <xsl:template match="/">
        <html lang="en">
            <body>
                <table style="width: 50%;border: 2px solid">
                    <tr>
                        <td colspan="2" style="text-align: center;padding: 10px">
                            Попуњава Завод
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style=";border-top: 2px solid;padding: 10px">
                            Број пријаве
                            <br/><br/>
                            <xsl:value-of select="z:zahtev_p1/z:sluzbeniDeo/z:brojPrijave/text()"/>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" style="width:50%;border-top: 2px solid;padding: 10px;border-right: 2px solid">
                            Датум пријема<br/><br/>
                            <xsl:value-of select="z:zahtev_p1/z:sluzbeniDeo/z:datumPrijema/text()"/>
                        </td>
                        <td colspan="1" style="width:50%;border-top: 2px solid;padding: 10px">
                            Признати датум подношења<br/><br/>
                            <xsl:value-of select="z:zahtev_p1/z:sluzbeniDeo/z:datumPodnosenja/text()"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-top: 2px solid;padding: 10px">
                            Печат и потпис<br/><br/><br/>
                            <xsl:value-of select="z:zahtev_p1/z:sluzbeniDeo/z:potpis"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-top: 2px solid;padding: 10px">
                            Република Србија<br/>
                            Завод за интелектуалну својину<br/>
                            Кнегиње Љубице број 5<br/>
                            11000 Београд
                        </td>
                    </tr>
                </table>
                <h3 style="margin-left: 50px">
                    ЗАХТЕВ ЗА ПРИЗНАЊЕ ПАТЕНТА
                </h3>
                <br/><br/><br/>
                <table style="width: 100%;border: 2px solid">
                    <tr>
                        <td colspan="4" style="padding: 10px">
                            <b>Поље број I              НАЗИВ ПРОНАЛАСКА</b>
                            <br/>
                            * Назив проналаска треба да јасно и сажето изражава суштину проналаска и не сме да садржи измишљене или комерцијалне називе, жигове, имена, шифре, уобичајене скраћенице за производе и сл.
                            <br/><br/><br/>
                            На српском језику: <xsl:value-of select="z:zahtev_p1/z:nazivPronalaska/z:nazivSrpski/text()"/>
                            <br/>
                            На енглеском језику: <xsl:value-of select="z:zahtev_p1/z:nazivPronalaska/z:nazivEngleski/text()"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 10px;border-top: 2px solid">
                            <b>Поље број II           ПОДНОСИЛАЦ ПРИЈАВЕ</b>  <span style="float:right;">☐ Подносилац пријаве је и проналазач</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="4" style="width: 30%;border-top: 1px solid">
                            Име и презиме / Пословно име: (презиме / пословно име уписати великим словима)
                            <p style="height: 100px"></p>
                            <xsl:value-of select="z:zahtev_p1/z:podnosilac/slo:ime/text()"/> <xsl:value-of select="z:zahtev_p1/z:podnosilac/slo:prezime/text()"/>
                        </td>
                        <td colspan="1" rowspan="4" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Улица и број, поштански број, место и држава:
                            <p style="height: 100px"></p>
                            <xsl:value-of select="z:zahtev_p1/z:podnosilac/slo:adresa/text()"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Број телефона:<xsl:value-of select="z:zahtev_p1/z:podnosilac/slo:telefon/text()"/>
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Број факса:<xsl:value-of select="z:zahtev_p1/z:podnosilac/slo:faks/text()"/>
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="2" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Е-пошта:<xsl:value-of select="z:zahtev_p1/z:podnosilac/slo:email/text()"/>
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="1" style="border-top: 1px solid">
                            Држављанство: <span style="float:right;">(попунити само за физичка лица)</span>
                            <p style="height: 10px"></p>
                            <xsl:value-of select="z:zahtev_p1/z:podnosilac/slo:drzavljanstvo/text()"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 10px;border-top: 1px solid">
                            ☐ <b>Остали подносиоци пријаве су наведени у додатном листу 1 у наставку овог захтева</b> <br/>
                            * Ако више лица подноси пријаву, потребно је одредити једно од тих лица као заједничког представника и доставити изјаву о заједничком представнику потписaну од стране свих подносилаца или именовати заједничког пуномоћника за заступање и доставити пуномоћје
                        </td>
                    </tr>
                </table>
                <br/>
                <table style="width: 100%;border: 2px solid">
                    <tr>
                        <td colspan="4" style="padding: 10px">
                            <b>Поље број III            ПРОНАЛАЗАЧ</b>
                            <br/>
                            (ако су сви проналазачи уједно и подносиоци пријаве, поље број III се не попуњава)
                            <br/>
                            * Ако сви подносиоци пријаве нису и проналазачи, доставља се изјава подносилаца пријаве о основу стицања права на подношење пријаве у односу на проналазаче који нису и подносиоци пријаве и у том случају у поље број III се уносе подаци о свим проналазачим
                            <br/><br/>
                            ☐   Проналазач не жели да буде наведен у пријави
                            <br/><br/>
                            (ако проналазач не жели да буде наведен у пријави поље број III се не попуњава)
                            <br/>
                            *Ако проналазач не жели да буде наведен у пријави, потребно је доставити потписану изјаву проналазача да не жели да буде наведен.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="4" style="width: 30%;border-top: 1px solid">
                            Име и презиме: (презиме уписати великим словима)
                            <p style="height: 100px"></p>
                        </td>
                        <td colspan="1" rowspan="4" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Улица и број, поштански број, место и држава:
                            <p style="height: 100px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Број телефона:
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Број факса:
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Е-пошта:
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 20px;border-top: 1px solid">
                            ☐ <b>Остали проналазачи су наведени у додатном листу 1 у наставку овог захтева</b>
                        </td>
                    </tr>
                </table>
                <br/>
                <table style="width: 100%;border: 2px solid">
                    <tr>
                        <td colspan="4" style="padding: 10px">
                            <b>
                                Поље број IV         ☐ ПУНОМОЋНИК ЗА ЗАСТУПАЊЕ      ☐ ПУНОМОЋНИК ЗА ПРИЈЕМ ПИСМЕНА       ☐ ЗАЈЕДНИЧКИ ПРЕДСТАВНИК
                            </b>
                            <br/><br/>
                            * Пуномоћник за заступање је лице које по овлашћењу подносиоца пријаве предузима радње у управном поступку у границама пуномоћја
                            <br/>
                            * Пуномоћник за пријем писмена је лице које је подносилац пријаве одредио као лице коме се  упућују сва писмена насловљена на подносиоца
                            <br/>
                            * Заједничи преставник је подносилац пријаве кога су подносиоци пријаве, у случају да пријаву подноси више лица, одредили да иступа у поступку пред органом ако подносиоци нису именовали заједничког пуномоћника за заступање
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="3" style="width: 50%;border-top: 1px solid">
                            Име и презиме / Пословно име: (презиме / пословно име уписати великим словима)
                            <p style="height: 70px"></p>
                        </td>
                        <td colspan="1" rowspan="3" style="width: 30%;border-top: 1px solid;border-left: 1px solid">
                            Улица и број, поштански број и место:
                            <p style="height: 70px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1" colspan="1" style="width: 20%;border-top: 1px solid;border-left: 1px solid">
                            Број телефона:
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1" colspan="1" style="width: 20%;border-top: 1px solid;border-left: 1px solid">
                            Е-пошта:
                            <p style="height: 30px"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 10px;border-top: 2px solid">
                            <b>Поље број V           АДРЕСА ЗА ДОСТАВЉАЊЕ</b>
                            <br/>
                            (ово поље се попуњава ако подносилац пријаве, заједнички представник или пуномоћник жели да се достављање поднесака врши на другој адреси од његове наведене адресе)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding:10px;border-top: 1px solid">
                            Улица и број, поштански број и место:
                            <xsl:value-of select="z:zahtev_p1/z:adresaDostavljanja/text()"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 10px;border-top: 2px solid">
                            <b>Поље број VI             НАЧИН ДОСТАВЉАЊА</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 10px">
                            <xsl:if test="z:zahtev_p1/z:elektronskaDostava/text()='true'">&#9632;</xsl:if>  Подносилац пријаве је сагласан да Завод врши достављање писмена искључиво електронским путем у форми електронског документа (у овом случају неопходна је регистрација на порталу „еУправе”)
                            <br/>
                            <xsl:if test="z:zahtev_p1/z:fizickaDostava/text()='true'">&#9632;</xsl:if>  Подносилац пријаве је сагласан да Завод врши достављање писмена у папирној форми
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 10px;border-top: 2px solid">
                            <b>Поље број VII
                                <xsl:if test="z:zahtev_p1/z:tipPrijave/z:dopunskaPrijava/text()='true'">&#9632;</xsl:if>ДОПУНСКА ПРИЈАВА
                                      <xsl:if test="z:zahtev_p1/z:tipPrijave/z:izdvojenaPrijava/text()='true'">&#9632;</xsl:if> ИЗДВОЈЕНА ПРИЈАВА</b>
                            <p>Број првобитне пријаве / основне пријаве, односно основног патента: </p>
                            <xsl:if test="z:zahtev_p1/z:tipPrijave/z:dopunskaPrijava/text()='true'">
                                <xsl:value-of select="z:zahtev_p1/z:tipPrijave/z:brojPrvobitnePrijave/text()"/>
                            </xsl:if>

                            <p>Датум подношења првобитнe пријаве / основне пријаве: </p>
                            <xsl:if test="z:zahtev_p1/z:tipPrijave/z:dopunskaPrijava/text()='true'">
                                <xsl:value-of select="z:zahtev_p1/z:tipPrijave/z:datumPrijema/text()"/>
                            </xsl:if>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 10px;border-top: 2px solid">
                            <b>Поље број VIII              ЗАХТЕВ ЗА ПРИЗНАЊЕ ПРАВА ПРВЕНСТВА ИЗ РАНИЈИХ ПРИЈАВА:</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="text-align: center;border-top: 1px solid;border-right: 1px solid;width: 25%">
                            Датум подношења раније пријаве
                        </td>
                        <td colspan="1" rowspan="1" style="text-align: center;border-top: 1px solid;border-right: 1px solid;width: 25%">
                        </td>
                        <td colspan="1" rowspan="1" style="text-align: center;border-top: 1px solid;border-right: 1px solid;width: 25%">
                            Број раније пријаве
                        </td>
                        <td colspan="1" rowspan="1" style="text-align: center;border-top: 1px solid;width: 25%">
                            Двословна ознака државе, регионалне или међународне организације
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;border-right: 1px solid;width: 25%">
                            1.
                        </td>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;border-right: 1px solid;width: 25%">
                        </td>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;border-right: 1px solid;width: 25%">
                        </td>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;width: 25%;height: 50px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;border-right: 1px solid;width: 25%">
                            2.
                        </td>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;border-right: 1px solid;width: 25%">
                        </td>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;border-right: 1px solid;width: 25%">
                        </td>
                        <td colspan="1" rowspan="1" style="border-top: 1px solid;width: 25%;height: 50px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border-top: 1px solid;padding: 10px">
                            ☐ <b>Подаци о осталим правима првенства су наведени у додатном листу 2 у наставку овог захтева</b>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>