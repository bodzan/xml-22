package ftn.xml.ServisPatenti.service;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import ftn.xml.ServisPatenti.repository.ZahtevP1Repository;
import ftn.xml.ServisPatenti.model.zahtev_p1.ZahtevP1;
import ftn.xml.ServisPatenti.repository.ZahtevP1Repository;
import ftn.xml.ServisPatenti.util.ZahtevP1XSLTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class ZahtevP1Service {

    @Autowired
    public ZahtevP1Repository zahtevP1Repository;

    @Autowired
    public ZahtevP1XSLTransformer zahtevP1XSLTransformer;

    public static final String HTML_FILE = "src/main/resources/xmlFiles/xhtml/zahtevP1.html";
    public static final String OUTPUT_FILE = "src/main/resources/xmlFiles/pdf/zahtevP1.pdf";
    public static final String XML_FILE = "src/main/resources/xmlFiles/zahtevP1save.xml";
    public void saveZahtevP1XML(String xml) throws Exception {

        File file = new File(XML_FILE);
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(ZahtevP1.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../docs/xml/zahtev_p1_schema.xsd"));
//        unmarshaller.setSchema(schema);

        ZahtevP1 zahtevP1 = (ZahtevP1) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/zahtevP1save.xml"));
        zahtevP1.getSluzbeniDeo().setBrojPrijave("P-" + UUID.randomUUID());
        String[] date = new SimpleDateFormat("yyyy-M-d").format(new Date()).split("-");
        zahtevP1.getSluzbeniDeo().getDatumPrijema().setYear(Integer.parseInt(date[0]));
        zahtevP1.getSluzbeniDeo().getDatumPrijema().setMonth(Integer.parseInt(date[1]));
        zahtevP1.getSluzbeniDeo().getDatumPrijema().setDay(Integer.parseInt(date[2]));
        zahtevP1.getSluzbeniDeo().getDatumPodnosenja().setYear(Integer.parseInt(date[0]));
        zahtevP1.getSluzbeniDeo().getDatumPodnosenja().setMonth(Integer.parseInt(date[1]));
        zahtevP1.getSluzbeniDeo().getDatumPodnosenja().setDay(Integer.parseInt(date[2]));
        zahtevP1Repository.saveZahtevP1Xml(zahtevP1);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Marshaller marshaller = context.createMarshaller();
        marshaller.setSchema(schema);

        marshaller.marshal(zahtevP1, os);

        try (OutputStream fileOS = new FileOutputStream(XML_FILE)) {
            os.writeTo(fileOS);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        System.out.println(os);

        xml = Files.readString(Path.of("src/main/resources/xmlFiles/zahtevP1save.xml"));
        zahtevP1XSLTransformer.generateHTML(xml);

        File htmlSource = new File(HTML_FILE);
        File pdfDest = new File(OUTPUT_FILE);
        // pdfHTML specific code
        ConverterProperties converterProperties = new ConverterProperties();
        HtmlConverter.convertToPdf(new FileInputStream(htmlSource), new FileOutputStream(pdfDest), converterProperties);
    }


    public Object findZahtevP1ById(String id) {
        return zahtevP1Repository.findZahtevP1Xml(id);
    }
}
