package ftn.xml.ServisPatenti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServisPatentiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServisPatentiApplication.class, args);
	}

}
