//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.06.10 at 06:06:41 PM CEST 
//


package ftn.xml.ServisPatenti.model.zahtev_p1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import lombok.Getter;
import lombok.Setter;
import ftn.xml.ServisPatenti.model.slozeni.TAdresa;
import ftn.xml.ServisPatenti.model.slozeni.TLicniPodaci;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sluzbeniDeo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="brojPrijave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="datumPrijema" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="datumPodnosenja" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="potpis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="nazivPronalaska">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nazivSrpski" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nazivEngleski" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="podnosilac">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.xml22.rs/slozeni/}TLicniPodaci">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="pronalazac" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.xml22.rs/slozeni/}TLicniPodaci">
 *                 &lt;attribute name="dodatniPronalazaci" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ovlascenje">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice>
 *                     &lt;element name="punomocnikZastupanje" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                     &lt;element name="punomocnikPrijem" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                     &lt;element name="predstavnik" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;/choice>
 *                   &lt;element name="podaci" type="{http://www.xml22.rs/slozeni/}TLicniPodaci"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="adresaDostavljanja" type="{http://www.xml22.rs/slozeni/}TAdresa"/>
 *         &lt;choice>
 *           &lt;element name="elektronskaDostava" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           &lt;element name="fizickaDostava" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;/choice>
 *         &lt;element name="tipPrijave">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice>
 *                     &lt;element name="izdvojenaPrijava" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                     &lt;element name="dopunskaPrijava" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;/choice>
 *                   &lt;element name="brojPrvobitnePrijave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="datumPodnosenja" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="pravaPrvenstva" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ranijaPrijava1" type="{http://www.xml22.rs/zahtev-p1/}TRanijaPrijava"/>
 *                   &lt;element name="ranijaPrijava2" type="{http://www.xml22.rs/zahtev-p1/}TRanijaPrijava"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="dodatnaPravaPrvenstva" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sluzbeniDeo",
    "nazivPronalaska",
    "podnosilac",
    "pronalazac",
    "ovlascenje",
    "elektronskaDostava",
    "fizickaDostava",
    "adresaDostavljanja",
    "tipPrijave",
    "pravaPrvenstva"
})
@XmlRootElement(name = "zahtev_p1")
@Getter
@Setter
public class ZahtevP1 {

    @XmlElement(required = true)
    protected SluzbeniDeo sluzbeniDeo;
    @XmlElement(required = true)
    protected NazivPronalaska nazivPronalaska;
    @XmlElement(required = true)
    protected Podnosilac podnosilac;
    protected Pronalazac pronalazac;
    @XmlElement(required = true)
    protected Ovlascenje ovlascenje;
    protected TAdresa adresaDostavljanja;
    protected Boolean elektronskaDostava;
    protected Boolean fizickaDostava;
    @XmlElement(required = true)
    protected TipPrijave tipPrijave;
    protected PravaPrvenstva pravaPrvenstva;

    /**
     * Gets the value of the sluzbeniDeo property.
     * 
     * @return
     *     possible object is
     *     {@link ZahtevP1 .SluzbeniDeo }
     *     
     */
    public SluzbeniDeo getSluzbeniDeo() {
        return sluzbeniDeo;
    }

    /**
     * Sets the value of the sluzbeniDeo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahtevP1 .SluzbeniDeo }
     *     
     */
    public void setSluzbeniDeo(SluzbeniDeo value) {
        this.sluzbeniDeo = value;
    }

    /**
     * Gets the value of the nazivPronalaska property.
     * 
     * @return
     *     possible object is
     *     {@link ZahtevP1 .NazivPronalaska }
     *     
     */
    public NazivPronalaska getNazivPronalaska() {
        return nazivPronalaska;
    }

    /**
     * Sets the value of the nazivPronalaska property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahtevP1 .NazivPronalaska }
     *     
     */
    public void setNazivPronalaska(NazivPronalaska value) {
        this.nazivPronalaska = value;
    }

    /**
     * Gets the value of the podnosilac property.
     * 
     * @return
     *     possible object is
     *     {@link ZahtevP1 .Podnosilac }
     *     
     */
    public Podnosilac getPodnosilac() {
        return podnosilac;
    }

    /**
     * Sets the value of the podnosilac property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahtevP1 .Podnosilac }
     *     
     */
    public void setPodnosilac(Podnosilac value) {
        this.podnosilac = value;
    }

    /**
     * Gets the value of the pronalazac property.
     * 
     * @return
     *     possible object is
     *     {@link ZahtevP1 .Pronalazac }
     *     
     */
    public Pronalazac getPronalazac() {
        return pronalazac;
    }

    /**
     * Sets the value of the pronalazac property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahtevP1 .Pronalazac }
     *     
     */
    public void setPronalazac(Pronalazac value) {
        this.pronalazac = value;
    }

    /**
     * Gets the value of the ovlascenje property.
     * 
     * @return
     *     possible object is
     *     {@link ZahtevP1 .Ovlascenje }
     *     
     */
    public Ovlascenje getOvlascenje() {
        return ovlascenje;
    }

    /**
     * Sets the value of the ovlascenje property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahtevP1 .Ovlascenje }
     *     
     */
    public void setOvlascenje(Ovlascenje value) {
        this.ovlascenje = value;
    }

    /**
     * Gets the value of the adresaDostavljanja property.
     * 
     * @return
     *     possible object is
     *     {@link TAdresa }
     *     
     */
    public TAdresa getAdresaDostavljanja() {
        return adresaDostavljanja;
    }

    /**
     * Sets the value of the adresaDostavljanja property.
     * 
     * @param value
     *     allowed object is
     *     {@link TAdresa }
     *     
     */
    public void setAdresaDostavljanja(TAdresa value) {
        this.adresaDostavljanja = value;
    }

    /**
     * Gets the value of the elektronskaDostava property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isElektronskaDostava() {
        return elektronskaDostava;
    }

    /**
     * Sets the value of the elektronskaDostava property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setElektronskaDostava(Boolean value) {
        this.elektronskaDostava = value;
    }

    /**
     * Gets the value of the fizickaDostava property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFizickaDostava() {
        return fizickaDostava;
    }

    /**
     * Sets the value of the fizickaDostava property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFizickaDostava(Boolean value) {
        this.fizickaDostava = value;
    }

    /**
     * Gets the value of the tipPrijave property.
     * 
     * @return
     *     possible object is
     *     {@link ZahtevP1 .TipPrijave }
     *     
     */
    public TipPrijave getTipPrijave() {
        return tipPrijave;
    }

    /**
     * Sets the value of the tipPrijave property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahtevP1 .TipPrijave }
     *     
     */
    public void setTipPrijave(TipPrijave value) {
        this.tipPrijave = value;
    }

    /**
     * Gets the value of the pravaPrvenstva property.
     * 
     * @return
     *     possible object is
     *     {@link ZahtevP1 .PravaPrvenstva }
     *     
     */
    public PravaPrvenstva getPravaPrvenstva() {
        return pravaPrvenstva;
    }

    /**
     * Sets the value of the pravaPrvenstva property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahtevP1 .PravaPrvenstva }
     *     
     */
    public void setPravaPrvenstva(PravaPrvenstva value) {
        this.pravaPrvenstva = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nazivSrpski" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nazivEngleski" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nazivSrpski",
        "nazivEngleski"
    })
    public static class NazivPronalaska {

        @XmlElement(required = true)
        protected String nazivSrpski;
        @XmlElement(required = true)
        protected String nazivEngleski;

        /**
         * Gets the value of the nazivSrpski property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNazivSrpski() {
            return nazivSrpski;
        }

        /**
         * Sets the value of the nazivSrpski property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNazivSrpski(String value) {
            this.nazivSrpski = value;
        }

        /**
         * Gets the value of the nazivEngleski property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNazivEngleski() {
            return nazivEngleski;
        }

        /**
         * Sets the value of the nazivEngleski property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNazivEngleski(String value) {
            this.nazivEngleski = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice>
     *           &lt;element name="punomocnikZastupanje" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *           &lt;element name="punomocnikPrijem" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *           &lt;element name="predstavnik" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;/choice>
     *         &lt;element name="podaci" type="{http://www.xml22.rs/slozeni/}TLicniPodaci"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "punomocnikZastupanje",
        "punomocnikPrijem",
        "predstavnik",
        "podaci"
    })
    public static class Ovlascenje {

        protected Boolean punomocnikZastupanje;
        protected Boolean punomocnikPrijem;
        protected Boolean predstavnik;
        @XmlElement(required = true)
        protected TLicniPodaci podaci;

        /**
         * Gets the value of the punomocnikZastupanje property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPunomocnikZastupanje() {
            return punomocnikZastupanje;
        }

        /**
         * Sets the value of the punomocnikZastupanje property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPunomocnikZastupanje(Boolean value) {
            this.punomocnikZastupanje = value;
        }

        /**
         * Gets the value of the punomocnikPrijem property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPunomocnikPrijem() {
            return punomocnikPrijem;
        }

        /**
         * Sets the value of the punomocnikPrijem property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPunomocnikPrijem(Boolean value) {
            this.punomocnikPrijem = value;
        }

        /**
         * Gets the value of the predstavnik property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPredstavnik() {
            return predstavnik;
        }

        /**
         * Sets the value of the predstavnik property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPredstavnik(Boolean value) {
            this.predstavnik = value;
        }

        /**
         * Gets the value of the podaci property.
         * 
         * @return
         *     possible object is
         *     {@link TLicniPodaci }
         *     
         */
        public TLicniPodaci getPodaci() {
            return podaci;
        }

        /**
         * Sets the value of the podaci property.
         * 
         * @param value
         *     allowed object is
         *     {@link TLicniPodaci }
         *     
         */
        public void setPodaci(TLicniPodaci value) {
            this.podaci = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.xml22.rs/slozeni/}TLicniPodaci">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Podnosilac
        extends TLicniPodaci
    {


    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ranijaPrijava1" type="{http://www.xml22.rs/zahtev-p1/}TRanijaPrijava"/>
     *         &lt;element name="ranijaPrijava2" type="{http://www.xml22.rs/zahtev-p1/}TRanijaPrijava"/>
     *       &lt;/sequence>
     *       &lt;attribute name="dodatnaPravaPrvenstva" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ranijaPrijava1",
        "ranijaPrijava2"
    })
    public static class PravaPrvenstva {

        @XmlElement(required = true)
        protected TRanijaPrijava ranijaPrijava1;
        @XmlElement(required = true)
        protected TRanijaPrijava ranijaPrijava2;
        @XmlAttribute
        protected Boolean dodatnaPravaPrvenstva;

        /**
         * Gets the value of the ranijaPrijava1 property.
         * 
         * @return
         *     possible object is
         *     {@link TRanijaPrijava }
         *     
         */
        public TRanijaPrijava getRanijaPrijava1() {
            return ranijaPrijava1;
        }

        /**
         * Sets the value of the ranijaPrijava1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link TRanijaPrijava }
         *     
         */
        public void setRanijaPrijava1(TRanijaPrijava value) {
            this.ranijaPrijava1 = value;
        }

        /**
         * Gets the value of the ranijaPrijava2 property.
         * 
         * @return
         *     possible object is
         *     {@link TRanijaPrijava }
         *     
         */
        public TRanijaPrijava getRanijaPrijava2() {
            return ranijaPrijava2;
        }

        /**
         * Sets the value of the ranijaPrijava2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link TRanijaPrijava }
         *     
         */
        public void setRanijaPrijava2(TRanijaPrijava value) {
            this.ranijaPrijava2 = value;
        }

        /**
         * Gets the value of the dodatnaPravaPrvenstva property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDodatnaPravaPrvenstva() {
            return dodatnaPravaPrvenstva;
        }

        /**
         * Sets the value of the dodatnaPravaPrvenstva property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDodatnaPravaPrvenstva(Boolean value) {
            this.dodatnaPravaPrvenstva = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.xml22.rs/slozeni/}TLicniPodaci">
     *       &lt;attribute name="dodatniPronalazaci" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Pronalazac
        extends TLicniPodaci
    {

        @XmlAttribute
        protected Boolean dodatniPronalazaci;

        /**
         * Gets the value of the dodatniPronalazaci property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public boolean isDodatniPronalazaci() {
            if (dodatniPronalazaci == null) {
                return false;
            } else {
                return dodatniPronalazaci;
            }
        }

        /**
         * Sets the value of the dodatniPronalazaci property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDodatniPronalazaci(Boolean value) {
            this.dodatniPronalazaci = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="brojPrijave" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="datumPrijema" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="datumPodnosenja" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="potpis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "brojPrijave",
        "datumPrijema",
        "datumPodnosenja",
        "potpis"
    })
    public static class SluzbeniDeo {

        @XmlElement(required = true)
        protected String brojPrijave;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datumPrijema;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datumPodnosenja;
        protected String potpis;

        /**
         * Gets the value of the brojPrijave property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrojPrijave() {
            return brojPrijave;
        }

        /**
         * Sets the value of the brojPrijave property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrojPrijave(String value) {
            this.brojPrijave = value;
        }

        /**
         * Gets the value of the datumPrijema property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatumPrijema() {
            return datumPrijema;
        }

        /**
         * Sets the value of the datumPrijema property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatumPrijema(XMLGregorianCalendar value) {
            this.datumPrijema = value;
        }

        /**
         * Gets the value of the datumPodnosenja property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatumPodnosenja() {
            return datumPodnosenja;
        }

        /**
         * Sets the value of the datumPodnosenja property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatumPodnosenja(XMLGregorianCalendar value) {
            this.datumPodnosenja = value;
        }

        /**
         * Gets the value of the potpis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPotpis() {
            return potpis;
        }

        /**
         * Sets the value of the potpis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPotpis(String value) {
            this.potpis = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice>
     *           &lt;element name="izdvojenaPrijava" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *           &lt;element name="dopunskaPrijava" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;/choice>
     *         &lt;element name="brojPrvobitnePrijave" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="datumPodnosenja" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "izdvojenaPrijava",
        "dopunskaPrijava",
        "brojPrvobitnePrijave",
        "datumPodnosenja"
    })
    public static class TipPrijave {

        protected Boolean izdvojenaPrijava;
        protected Boolean dopunskaPrijava;
        @XmlElement(required = true)
        protected String brojPrvobitnePrijave;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datumPodnosenja;

        /**
         * Gets the value of the izdvojenaPrijava property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIzdvojenaPrijava() {
            return izdvojenaPrijava;
        }

        /**
         * Sets the value of the izdvojenaPrijava property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIzdvojenaPrijava(Boolean value) {
            this.izdvojenaPrijava = value;
        }

        /**
         * Gets the value of the dopunskaPrijava property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDopunskaPrijava() {
            return dopunskaPrijava;
        }

        /**
         * Sets the value of the dopunskaPrijava property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDopunskaPrijava(Boolean value) {
            this.dopunskaPrijava = value;
        }

        /**
         * Gets the value of the brojPrvobitnePrijave property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrojPrvobitnePrijave() {
            return brojPrvobitnePrijave;
        }

        /**
         * Sets the value of the brojPrvobitnePrijave property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrojPrvobitnePrijave(String value) {
            this.brojPrvobitnePrijave = value;
        }

        /**
         * Gets the value of the datumPodnosenja property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatumPodnosenja() {
            return datumPodnosenja;
        }

        /**
         * Sets the value of the datumPodnosenja property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatumPodnosenja(XMLGregorianCalendar value) {
            this.datumPodnosenja = value;
        }

    }

}
