//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.06.10 at 06:06:41 PM CEST 
//


package ftn.xml.ServisPatenti.model.slozeni;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the rs.xml22.slozeni package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: rs.xml22.slozeni
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TLicniPodaci.Ime }
     * 
     */
    public TLicniPodaci.Ime createTLicniPodaciIme() {
        return new TLicniPodaci.Ime();
    }

    /**
     * Create an instance of {@link TLicniPodaci.Email }
     * 
     */
    public TLicniPodaci.Email createTLicniPodaciEmail() {
        return new TLicniPodaci.Email();
    }

    /**
     * Create an instance of {@link TLicniPodaci }
     * 
     */
    public TLicniPodaci createTLicniPodaci() {
        return new TLicniPodaci();
    }

    /**
     * Create an instance of {@link TAdresa }
     * 
     */
    public TAdresa createTAdresa() {
        return new TAdresa();
    }

    /**
     * Create an instance of {@link TLicniPodaci.Prezime }
     * 
     */
    public TLicniPodaci.Prezime createTLicniPodaciPrezime() {
        return new TLicniPodaci.Prezime();
    }

}
