<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:z="http://www.xml22.rs/zahtev-a1/"
                xmlns:slo="http://www.xml22.rs/slozeni/" version="2.0">
    <xsl:template match="/">
        <html lang="en">
            <body>
                <table style="border: 1px solid">
                    <tr>
                        <td colspan="2" style="border-bottom: 2px solid">
                            <p>
                                <b>ЗАВОД ЗА ИНТЕЛЕКТУАЛНУ СВОЈИНУ</b><span style="float: right"><b>ОБРАЗАЦ А-1</b></span>
                                <br/>
                                Београд, Кнегиње Љубице 5
                                <br/>
                                <h4 style="padding: 10px;margin-left: 80px">ЗАХТЕВ ЗА УНОШЕЊЕ У ЕВИДЕНЦИЈУ И ДЕПОНОВАЊЕ АУТОРСКИХ ДЕЛА</h4>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 10px">
                            <p>1) Подносилац - име, презиме, адреса и држављанство аутора или другог носиоца ауторског права ако је подносилац физичко лице, односно пословно име и седиште носиоца ауторског права ако је подносилац правно лице*:</p>
                            <br/><br/>
                            <xsl:value-of select="z:zahtev_a1/z:podnosilac/slo:ime[text()]"/>,
                            <xsl:value-of select="z:zahtev_a1/z:podnosilac/slo:prezime[text()]"/>,
                            <xsl:value-of select="z:zahtev_a1/z:podnosilac/slo:drzavljanstvo[text()]"/>,
                            <xsl:value-of select="z:zahtev_a1/z:podnosilac/slo:adresa[text()]"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" style="width:50%;border: 1px solid">
                            телефон: <xsl:value-of select="z:zahtev_a1/z:podnosilac/slo:telefon[text()]"/>
                        </td>
                        <td colspan="1" style="width:50%;border: 1px solid">
                            e-mail: <xsl:value-of select="z:zahtev_a1/z:podnosilac/slo:email[text()]"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            2) Псеудоним или знак аутора, (ако га има):
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            3) Име, презиме и адреса пуномоћника, ако се пријава подноси преко пуномоћника:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            4) Наслов ауторског дела, односно алтернативни наслов, ако га има, по коме ауторско дело може да се идентификује*:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            5) Подаци о наслову ауторског дела на коме се заснива дело прераде, ако је у питању ауторско дело прераде, као и податак о аутору изворног дела:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            6) Подаци о врсти ауторског дела (књижевно дело, музичко дело, ликовно дело, рачунарски програм и др.) *:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            7) Подаци о форми записа ауторског дела (штампани текст, оптички диск и слично) *:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            8) Подаци о аутору ако подносилац пријаве из тачке 1. овог захтева није аутор и то: презиме, име, адреса и држављанство аутора (групе аутора или коаутора), а ако су у питању један или више аутора који нису живи, имена аутора и године смрти аутора а ако је у питању ауторско дело анонимног аутора навод да је ауторско дело дело анонимног аутора:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            9) Податак да ли је у питању ауторско дело створено у радном односу:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            10) Начин коришћења ауторског дела или намеравани начин коришћења ауторског дела:
                            <br/><br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            11)<p style="float: right;width: 50%">____________________________________<br/>Подносилац пријаве, носилац права
                            (место за потпис физичког лица, односно потпис
                            заступника правног лица или овлашћеног представника
                            у правном лицу)*
                        </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            12) Прилози који се подносе уз захтев:<br/><br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b style="margin-left: 50px">ПОПУЊАВА ЗАВОД:</b>
                            <br/><br/>
                            Прилози уз пријаву:<br/><br/>
                            <xsl:if test="z:zahtev_a1/z:sluzbeniDeo/z:prilozi/z:opisDela/text()='true'">&#9632;</xsl:if>
                            <xsl:if test="z:zahtev_a1/z:sluzbeniDeo/z:prilozi/z:opisDela/text()='false'">&#x25A2;</xsl:if>
                            опис ауторског дела (ако је дело поднето на оптичком диску);<br/><br/>
                            <xsl:if test="z:zahtev_a1/z:sluzbeniDeo/z:prilozi/z:primerDela/text()='true'">&#9632;</xsl:if>
                            <xsl:if test="z:zahtev_a1/z:sluzbeniDeo/z:prilozi/z:primerDela/text()='false'">&#x25A2;</xsl:if>
                            пример ауторског дела (слика, видео запис, аудио запис)<br/><br/>
                            <p style="height: 300px"></p>

                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%"></td>

                        <td style="border-top: 1px solid;border-right: 1px solid;border-left: 1px solid">
                            <p>
                                Број пријаве
                                <br/><br/>
                                <xsl:value-of select="z:zahtev_a1/z:sluzbeniDeo/z:brojPrijave/text()"/>
                                <br/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%"></td>
                        <td style="border-top: 1px solid;border-right: 1px solid;border-left: 1px solid">
                            <p>
                                Датум подношења: <xsl:value-of select="z:zahtev_a1/z:sluzbeniDeo/z:datum/text()"/>
                                <br/><br/><br/>
                            </p>
                        </td>
                    </tr>
                </table>
                <p>Рубрике у захтеву А-1 које су означене са * морају да буду обавезно попуњене.</p>

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>