package ftn.xml.ServisPrava.controller;

import ftn.xml.ServisPrava.service.ZahtevA1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/api/zahteviPrava")
public class ZahtevA1Controller {

    @Autowired
    public ZahtevA1Service zahtevA1Service;


    @GetMapping(value = "/read/{id}", produces = "application/xml")
    public ResponseEntity<Object> findZahtevA1(@PathVariable String id) {

        try {
            Object zahtevA1 = zahtevA1Service.findZahtevA1ById(id);
            return new ResponseEntity<>(zahtevA1, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/create")
    public ResponseEntity<String> writeZahtevA1Xml(@RequestBody String xml) {
        try {

            zahtevA1Service.saveZahtevA1XML(xml);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }

    }

//    @GetMapping(value = "email")
//    public ResponseEntity<String> sendEmailWithAttachment() {
//        sendEmail();
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

//    private void sendEmail() {
//
//        String korisnik = "ggwp012@gmail.com";
//        EmailTemplate  emailTemplate = new EmailTemplate();
//        emailTemplate.setAttachmentPath("D:\\Users\\HpZbook15\\Desktop\\xml-2021\\PortalZaImunizaciju\\src\\main\\resources\\xmlFiles\\pdf\\obrazac-saglasnosti-za-imunizaciju.pdf");
//        emailTemplate.setBody("HTML kreiranog zahteva.");
//        emailTemplate.setSentFrom("Servis organa vlasti");
//        emailTemplate.setSubject("Uspesno kreiran zahtev");
//        emailTemplate.setSendTo(korisnik);
//
//
//        RestTemplate restTemplate = new RestTemplate();
//        HttpEntity<EmailTemplate> entity = new HttpEntity<>(emailTemplate);
//
//
//        restTemplate.postForEntity("http://localhost:6969/email/send/attachemail", entity, String.class);
//    }
}
