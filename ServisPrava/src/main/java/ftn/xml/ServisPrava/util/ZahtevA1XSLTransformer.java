package ftn.xml.ServisPrava.util;

import org.springframework.context.annotation.Configuration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

@Configuration
public class ZahtevA1XSLTransformer {

    private static DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

    private static TransformerFactory transformerFactory = TransformerFactory.newInstance();

    public ZahtevA1XSLTransformer() {
        documentFactory.setNamespaceAware(true);
        documentFactory.setIgnoringComments(true);
        documentFactory.setIgnoringElementContentWhitespace(true);
    }

    private org.w3c.dom.Document buildDocument(String xml) {

        org.w3c.dom.Document document = null;
        try {

            DocumentBuilder builder = documentFactory.newDocumentBuilder();
            InputStream targetStream = new ByteArrayInputStream(xml.getBytes());
            document = builder.parse(targetStream);

            if (document != null)
                System.out.println("[INFO] File parsed with no errors.");
            else
                System.out.println("[WARN] Document is null.");

        } catch (Exception e) {
            return null;

        }

        return document;
    }


    public void generateXML(String xml) throws FileNotFoundException {

        try {

            // Initialize Transformer instance
            StreamSource transformSource = new StreamSource(new File("src/main/resources/xmlFiles/xsl/zahtevA1_html.xsl"));
            Transformer transformer = transformerFactory.newTransformer(transformSource);
            transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            // Generate XHTML
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");

            // Transform DOM to HTML
            DOMSource source = new DOMSource(buildDocument(xml));
            StreamResult result = new StreamResult(new FileOutputStream("src/main/resources/xmlFiles/xhtml/zahtevA1_gen.xml"));
            transformer.transform(source, result);


        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    public void generateHTML(String xml) throws FileNotFoundException {

        try {
            // Initialize Transformer instance
            StreamSource transformSource = new StreamSource(new File("src/main/resources/xmlFiles/xsl/zahtevA1_html.xsl"));
            Transformer transformer = transformerFactory.newTransformer(transformSource);
            transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            // Generate XHTML
            transformer.setOutputProperty(OutputKeys.METHOD, "xhtml");

            // Transform DOM to HTML
            DOMSource source = new DOMSource(buildDocument(xml));
            StreamResult result = new StreamResult(new FileOutputStream("src/main/resources/xmlFiles/xhtml/zahtevA1_html.html"));
            transformer.transform(source, result);


        } catch (TransformerFactoryConfigurationError | TransformerException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        try {
            String xml = Files.readString(Path.of("src/main/resources/xmlFiles/zahtevA1save.xml"));
            new ZahtevA1XSLTransformer().generateHTML(xml);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
