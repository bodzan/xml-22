package ftn.xml.ServisPrava.repository;

import ftn.xml.ServisPrava.model.zahtevA1.ZahtevA1;
import ftn.xml.ServisPrava.util.MetadataExtractor;
import ftn.xml.ServisPrava.util.RdfDbConnectionUtils;
import ftn.xml.ServisPrava.util.XmlDbConnectionUtils;
import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class ZahtevA1Repository {

    public final String PRAVA_COLLECTION_NAME = "/db/prava";
    public final String PRAVA_NAMED_GRAPH_URI = "/prava/metadata";


    public Object findZahtevA1Xml(String id) {
        String collectionId = PRAVA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(ZahtevA1.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../docs/xml/zahtev_a1_schema.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            ZahtevA1 zahtevA1 = (ZahtevA1) unmarshaller.unmarshal(res.getContentAsDOM());

            JAXBContext zahtevA1Context = JAXBContext.newInstance(ZahtevA1.class);

            // create an instance of `Unmarshaller`
            Marshaller marshaller = zahtevA1Context.createMarshaller();

            SchemaFactory saglasnostsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema saglasnostschema = saglasnostsf.newSchema(new File("../docs/xml/zahtev_a1_schema.xsd"));
            marshaller.setSchema(saglasnostschema);

            marshaller.marshal(zahtevA1, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);
            xml = xmlResource.getContent();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }

    public Object findZahtevA1Class(String id) {
        String collectionId = PRAVA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        ZahtevA1 zahtevA1 = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(ZahtevA1.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../docs/xml/zahtev_a1_schema.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            zahtevA1 = (ZahtevA1) unmarshaller.unmarshal(res.getContentAsDOM());



        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return zahtevA1;
    }


    public void saveZahtevA1Xml(ZahtevA1 zahtevA1) throws Exception {
        String collectionId = PRAVA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

            extractAndSaveMetadata();
            readZahtevA1Metadata();

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);


            res = (XMLResource) col.createResource(zahtevA1.getSluzbeniDeo().getBrojPrijave(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(ZahtevA1.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(zahtevA1, os);

            res.setContent(os);

            col.storeResource(res);



        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }


    private void extractAndSaveMetadata() throws Exception {
        MetadataExtractor extractor = new MetadataExtractor();
        String xml = Files.readString(Path.of("src/main/resources/xmlFiles/zahtevA1save.xml"));
        InputStream in = new ByteArrayInputStream(xml.getBytes());

        OutputStream out = new FileOutputStream("src/main/resources/xmlFiles/rdf/metadata.rdf");

        extractor.extractMetadata(in, out);

        RdfDbConnectionUtils.writeMetadataToDatabase(PRAVA_NAMED_GRAPH_URI);
    }

    private void readZahtevA1Metadata() throws Exception {
        RdfDbConnectionUtils.loadMetadataFromDatabase(PRAVA_NAMED_GRAPH_URI);
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }

}
