package ftn.xml.ServisPrava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServisPravaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServisPravaApplication.class, args);
	}

}
