package ftn.xml.ServisPrava.service;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import ftn.xml.ServisPrava.model.zahtevA1.ZahtevA1;
import ftn.xml.ServisPrava.repository.ZahtevA1Repository;
import ftn.xml.ServisPrava.util.ZahtevA1XSLTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmldb.api.modules.XMLResource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class ZahtevA1Service {

    @Autowired
    public ZahtevA1Repository zahtevA1Repository;

    @Autowired
    public ZahtevA1XSLTransformer zahtevA1XSLTransformer;

    public static final String HTML_FILE = "src/main/resources/xmlFiles/xhtml/zahtevA1_html.html";
    public static final String OUTPUT_FILE = "src/main/resources/xmlFiles/pdf/zahtevA1.pdf";
    public static final String XML_FILE = "src/main/resources/xmlFiles/zahtevA1save.xml";


    public void saveZahtevA1XML(String xml) throws Exception {

        File file = new File("src/main/resources/xmlFiles/zahtevA1save.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(ZahtevA1.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../docs/xml/zahtev_a1_schema.xsd"));
        unmarshaller.setSchema(schema);

        ZahtevA1 zahtevA1 = (ZahtevA1) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/zahtevA1save.xml"));
        zahtevA1.getSluzbeniDeo().setBrojPrijave("A-" + UUID.randomUUID());
        String[] date = new SimpleDateFormat("yyyy-M-d").format(new Date()).split("-");
        zahtevA1.getSluzbeniDeo().getDatum().setYear(Integer.parseInt(date[0]));
        zahtevA1.getSluzbeniDeo().getDatum().setMonth(Integer.parseInt(date[1]));
        zahtevA1.getSluzbeniDeo().getDatum().setDay(Integer.parseInt(date[2]));
        zahtevA1Repository.saveZahtevA1Xml(zahtevA1);

        // create an instance of `Unmarshaller`
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Marshaller marshaller = context.createMarshaller();

        SchemaFactory a1_sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema a1_schema = a1_sf.newSchema(new File("../docs/xml/zahtev_a1_schema.xsd"));
        marshaller.setSchema(a1_schema);

        marshaller.marshal(zahtevA1, os);

        try (OutputStream fileOS = new FileOutputStream(XML_FILE)) {
            os.writeTo(fileOS);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println(os);

        xml = Files.readString(Path.of("src/main/resources/xmlFiles/zahtevA1save.xml"));
        zahtevA1XSLTransformer.generateHTML(xml);

        File htmlSource = new File(HTML_FILE);
        File pdfDest = new File(OUTPUT_FILE);
        // pdfHTML specific code
        ConverterProperties converterProperties = new ConverterProperties();
        HtmlConverter.convertToPdf(new FileInputStream(htmlSource), new FileOutputStream(pdfDest), converterProperties);
    }


    public Object findZahtevA1ById(String id) {
        return zahtevA1Repository.findZahtevA1Xml(id);
    }
}
