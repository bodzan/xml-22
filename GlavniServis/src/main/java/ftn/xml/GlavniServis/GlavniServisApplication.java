package ftn.xml.GlavniServis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlavniServisApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlavniServisApplication.class, args);
	}

}
