package ftn.xml.GlavniServis.service;

import ftn.xml.GlavniServis.model.resenje.Resenje;
import ftn.xml.GlavniServis.repository.ResenjeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class ResenjeService {

    @Autowired
    public ResenjeRepository resenjeRepository;

    public Object findZahtevZ1ById(String id) {
        return resenjeRepository.findResenjeXml(id);
    }
    
    public void saveZahtevZ1XML(String xml) throws Exception {

        File file = new File("src/main/resources/xmlFiles/resenjeSave.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(Resenje.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../docs/xml/resenje_schema.xsd"));
        unmarshaller.setSchema(schema);

        Resenje resenje1 = (Resenje) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/resenjeSave.xml"));
        //resenje1.getPodaciZavod().setBrojPrijave("Z-" + UUID.randomUUID());
        String[] date = new SimpleDateFormat("yyyy-M-d").format(new Date()).split("-");
        resenje1.getDatumObrade().setYear(Integer.parseInt(date[0]));
        resenje1.getDatumObrade().setMonth(Integer.parseInt(date[1]));
        resenje1.getDatumObrade().setDay(Integer.parseInt(date[2]));
        resenjeRepository.saveResenjeXml(resenje1);
    }
    
}
