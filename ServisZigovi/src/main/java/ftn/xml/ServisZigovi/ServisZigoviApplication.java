package ftn.xml.ServisZigovi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServisZigoviApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServisZigoviApplication.class, args);
	}

}
