package ftn.xml.ServisZigovi.repository;

import ftn.xml.ServisZigovi.model.zahtevZ1.ZahtevZ1;
import ftn.xml.ServisZigovi.util.MetadataExtractor;
import ftn.xml.ServisZigovi.util.RdfDbConnectionUtils;
import ftn.xml.ServisZigovi.util.XmlDbConnectionUtils;

import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class ZahtevZ1Repository {

    public final String ZIGOVI_COLLECTION_NAME = "/db/zigovi";
    public final String ZIGOVI_NAMED_GRAPH_URI = "/zigovi/metadata";

    public Object findZahtevZ1Xml(String id) {
        String collectionId = ZIGOVI_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(ZahtevZ1.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../docs/xml/zahtev_z1_schema.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            ZahtevZ1 zahtevZ1 = (ZahtevZ1) unmarshaller.unmarshal(res.getContentAsDOM());
//
            JAXBContext zahtevZ1Context = JAXBContext.newInstance(ZahtevZ1.class);

            // create an instance of `Unmarshaller`
            Marshaller marshaller = zahtevZ1Context.createMarshaller();

            SchemaFactory zahtevz1sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema zahtevz1schema = zahtevz1sf.newSchema(new File("../docs/xml/zahtev_z1_schema.xsd"));
            marshaller.setSchema(zahtevz1schema);

            marshaller.marshal(zahtevZ1, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);
            xml = xmlResource.getContent();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }

    public Object findZahtevZ1Class(String id) {
        String collectionId = ZIGOVI_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        ZahtevZ1 zahtevZ1 = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(ZahtevZ1.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/zahtevz1sema.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            zahtevZ1 = (ZahtevZ1) unmarshaller.unmarshal(res.getContentAsDOM());



        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return zahtevZ1;
    }

    public void saveZahtevZ1Xml(ZahtevZ1 zahtevZ1) throws Exception {
        String collectionId = ZIGOVI_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

//            extractAndSaveMetadata();
//            readSaglasnostMetadata();

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);


            res = (XMLResource) col.createResource(zahtevZ1.getPodaciZavod().getBrojPrijave().getValue(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(ZahtevZ1.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(zahtevZ1, os);

            res.setContent(os);

            col.storeResource(res);



        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }

    private void extractAndSaveMetadata() throws Exception {
        MetadataExtractor extractor = new MetadataExtractor();
        String xml = Files.readString(Path.of("src/main/resources/xmlFiles/xhtml/zahtevZ1.xml"));
        InputStream in = new ByteArrayInputStream(xml.getBytes());

        OutputStream out = new FileOutputStream("src/main/resources/xmlFiles/rdf/metadata.rdf");

        extractor.extractMetadata(in, out);

        RdfDbConnectionUtils.writeMetadataToDatabase(ZIGOVI_NAMED_GRAPH_URI);
    }

    private void readZahtevZ1Metadata() throws Exception {
        RdfDbConnectionUtils.loadMetadataFromDatabase(ZIGOVI_NAMED_GRAPH_URI);
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }
    
}
