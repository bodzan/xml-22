package ftn.xml.ServisZigovi.service;


import ftn.xml.ServisZigovi.model.zahtevZ1.ZahtevZ1;
import ftn.xml.ServisZigovi.repository.ZahtevZ1Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class ZahtevZ1Service {

    @Autowired
    public ZahtevZ1Repository zahtevZ1Repository;

    public Object findZahtevZ1ById(String id) {
        return zahtevZ1Repository.findZahtevZ1Xml(id);
    }
    
    public void saveZahtevZ1XML(String xml) throws Exception {

        File file = new File("src/main/resources/xmlFiles/zahtevZ1save.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(ZahtevZ1.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../docs/xml/zahtev_z1_schema.xsd"));
        unmarshaller.setSchema(schema);

        ZahtevZ1 zahtevZ1 = (ZahtevZ1) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/zahtevZ1save.xml"));
        zahtevZ1.getPodaciZavod().getBrojPrijave().setValue("Z-" + UUID.randomUUID());
        String[] date = new SimpleDateFormat("yyyy-M-d").format(new Date()).split("-");
        zahtevZ1.getPodaciZavod().getDatumPodnosenja().setYear(Integer.parseInt(date[0]));
        zahtevZ1.getPodaciZavod().getDatumPodnosenja().setMonth(Integer.parseInt(date[1]));
        zahtevZ1.getPodaciZavod().getDatumPodnosenja().setDay(Integer.parseInt(date[2]));
        zahtevZ1Repository.saveZahtevZ1Xml(zahtevZ1);
    }
}
