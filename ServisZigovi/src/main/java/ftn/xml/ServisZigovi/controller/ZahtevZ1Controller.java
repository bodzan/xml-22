package ftn.xml.ServisZigovi.controller;

import ftn.xml.ServisZigovi.service.ZahtevZ1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/api/zahteviZigovi")
public class ZahtevZ1Controller {

    @Autowired
    public ZahtevZ1Service zahtevZ1Service;
    
    @GetMapping(value = "/read/{id}", produces = "application/xml")
    public ResponseEntity<Object> findZahtevZ1(@PathVariable String id) {

        try {
            Object zahtevZ1 = zahtevZ1Service.findZahtevZ1ById(id);
            return new ResponseEntity<>(zahtevZ1, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/create")
    public ResponseEntity<String> writeZahtevZ1Xml(@RequestBody String xml) {
        try {

            zahtevZ1Service.saveZahtevZ1XML(xml);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }

    }

    //    @GetMapping(value = "email")
//    public ResponseEntity<String> sendEmailWithAttachment() {
//        sendEmail();
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

//    private void sendEmail() {
//
//        String korisnik = "ggwp012@gmail.com";
//        EmailTemplate  emailTemplate = new EmailTemplate();
//        emailTemplate.setAttachmentPath("D:\\Users\\HpZbook15\\Desktop\\xml-2021\\PortalZaImunizaciju\\src\\main\\resources\\xmlFiles\\pdf\\obrazac-saglasnosti-za-imunizaciju.pdf");
//        emailTemplate.setBody("HTML kreiranog zahteva.");
//        emailTemplate.setSentFrom("Servis organa vlasti");
//        emailTemplate.setSubject("Uspesno kreiran zahtev");
//        emailTemplate.setSendTo(korisnik);
//
//
//        RestTemplate restTemplate = new RestTemplate();
//        HttpEntity<EmailTemplate> entity = new HttpEntity<>(emailTemplate);
//
//
//        restTemplate.postForEntity("http://localhost:6969/email/send/attachemail", entity, String.class);
//    }
}
