import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class P1Service {
  private api: string
  constructor(private http: HttpClient) {
    this.api = "http://localhost:8086/";
  }

  public createNewP1Xml(xml: string): Observable<any> {
    return  this.http.post(this.api + "api/zahteviPatenti/create", xml, {headers: {'content-type': 'application/xml'}});
  }
}
