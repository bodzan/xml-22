import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private api: string

  constructor(
    private http: HttpClient,
    private router: Router
  ) { 
    this.api = "http://localhost:8085/";
  }

  login(auth: any): Observable<any> {
    // console.log(auth);
    const body: string = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<k:login xmlns:vc=\"http://www.w3.org/2007/XMLSchema-versioning\"\n" +
      " xmlns:k=\"http://www.ftn.uns.ac.rs/korisnik\"\n" +
      " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
      "    <k:username>" + auth.username + "</k:username>\n" +
      "    <k:password>" + auth.password + "</k:password>\n" +
      "</k:login>\n"
    console.log(body);
    return this.http.post(this.api + 'auth/login', body, {headers: {'content-type': 'application/xml'}});
  }

  registerGradjanin(auth: any): Observable<any> {
    const body: string = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<korisnik xmlns=\"http://www.ftn.uns.ac.rs/korisnik\"\n" +
      " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
      ">\n" +
      "    <korisnicko_ime>" + auth.username + "</korisnicko_ime>\n" +
      "    <lozinka>" + auth.password + "</lozinka>\n" +
      "    <ime>" + auth.firstName + ' ' + auth.lastName + "</ime>\n" +
      "    <tip_korisnika>korisnik</tip_korisnika>\n" +
      "    <uloga>gradjanin</uloga>\n" +
      "    <jmbg>" + auth.jmbg + "</jmbg>\n" +
      "</korisnik>\n"
    return this.http.post(this.api + 'auth/register', body, {headers: {'content-type': 'application/xml'}});
  }

  logout(): void {
    sessionStorage.clear();
    this.router.navigate(['']);

  }

}
