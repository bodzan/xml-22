import { Injectable } from '@angular/core';

declare const Xonomy: any
@Injectable({
  providedIn: 'root'
})
export class XonomyService {

  constructor() { }

  addFizickaDostavaAndAdresaDostaljanja(htmlID: any, param: any) {
    Xonomy.newElementChild(htmlID, param[0]);
    Xonomy.newElementChild(htmlID, param[1]);
  }

  // @ts-ignore
  public p1Specification = {
    elements: {
      "z:zahtev_p1": {
        menu: [{
          caption: "Odaberi tip dostave",
          menu: [{
            caption: "dodaj <elektronskaDostava>",
            action: Xonomy.newElementChild,
            actionParameter: "<z:elektronskaDostava xmlns:z=\"http://www.xml22.rs/zahtev-p1/\">true</z:elektronskaDostava>"
          },
            {
              caption: "dodaj <fizickaDostava> i <adresaDostavljanja>",
              action: this.addFizickaDostavaAndAdresaDostaljanja,
              actionParameter: ["<z:fizickaDostava xmlns:z=\"http://www.xml22.rs/zahtev-p1/\">true</z:fizickaDostava>",
                "<z:adresaDostavljanja xmlns:z=\"http://www.xml22.rs/zahtev-p1/\">" +
              "<slo:ulica xmlns:slo=\"http://www.xml22.rs/slozeni/\"></slo:ulica>" +
                "<slo:brojUlice xmlns:slo=\"http://www.xml22.rs/slozeni/\"></slo:brojUlice>" +
                "<slo:postanskiBroj xmlns:slo=\"http://www.xml22.rs/slozeni/\"></slo:postanskiBroj>" +
                "<slo:mesto xmlns:slo=\"http://www.xml22.rs/slozeni/\"></slo:mesto>" +
                "<slo:drzava xmlns:slo=\"http://www.xml22.rs/slozeni/\"></slo:drzava></z:adresaDostavljanja>"]
            }],
          hideIf: function (jsElem: any) {
            return jsElem.hasChildElement("z:elektronskaDostava") || jsElem.hasChildElement("z:fizickaDostava")
          }
        }]
      },
      "z:sluzbeniDeo": {
        oneliner: true
      },
      "z:nazivPronalaska": {
      },
      "z:podnosilac": {
        hasText: false
      },
      "z:ovlascenje": {
        hasText: false

      },
      "z:adresaDostavljanja": {
        menu: [{
          caption: "delete element",
          action: Xonomy.deleteElement,
        }],
        mustBeBefore: ["z:tipPrijave"]
      },
      "z:elektronskaDostava": {
        menu: [{
          caption: "delete element",
          action: Xonomy.deleteElement
        }],
        oneliner: true,
        mustBeBefore: ["z:tipPrijave"]
      },
      "z:fizickaDostava": {
        menu: [{
          caption: "delete element",
          action: Xonomy.deleteElement
        }],
        oneliner: true,
        mustBeBefore: ["z:tipPrijave"]
      },
      "z:tipPrijave": {
        menu: [{
          caption: "dodaj izdvojena prijava",
          action: Xonomy.newElementChild,
          actionParameter: "<z:izdvojenaPrijava xmlns:z=\"http://www.xml22.rs/zahtev-p1/\">true</z:izdvojenaPrijava>"
        },
          {
            caption: "dodaj dopunska prijava",
            action: Xonomy.newElementChild,
            actionParameter: "<z:dopunskaPrijava xmlns:z=\"http://www.xml22.rs/zahtev-p1/\">true</z:dopunskaPrijava>"
          }
          ]
      },
      "z:nazivSrpski": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "z:nazivEngleski": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:ime": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:prezime": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:ulica": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:brojUlice": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:postanskiBroj": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:mesto": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:drzava": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:email": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "slo:telefon": {
        oneliner: true,
        asker: Xonomy.askString,
        hasText: true
      },
      "z:punomocnikZastupanje": {
        oneliner: true,
        asker: Xonomy.askPicklist,
        askerParameter: [{
          value: "true", caption: "true"
        },{
          value: "false", caption: "false"
        }]
      },
      "z:izdvojenaPrijava": {
        oneliner: true,
        mustBeBefore: ["z:brojPrvobitnePrijave", "z:datumPodnosenja"]
      },
      "z:dopunskaPrijava": {
        oneliner: true,
        mustBeBefore: ["z:brojPrvobitnePrijave", "z:datumPodnosenja"]
      },
      "z:brojPrvobitnePrijave": {
        oneliner: true
      },
      "z:datumPodnosenja": {
        oneliner: true
      }
    }
  }
}
