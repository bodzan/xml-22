import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewA1PageComponent } from './new-a1-page.component';

describe('NewA1PageComponent', () => {
  let component: NewA1PageComponent;
  let fixture: ComponentFixture<NewA1PageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewA1PageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewA1PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
