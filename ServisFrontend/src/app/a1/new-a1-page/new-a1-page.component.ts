import {AfterViewInit, Component, OnInit} from '@angular/core';
import {XonomyService} from "../../service/xonomy.service";
import {A1Service} from "../../service/a1.service";

declare const Xonomy: any
@Component({
  selector: 'app-new-a1-page',
  templateUrl: './new-a1-page.component.html',
  styleUrls: ['./new-a1-page.component.css']
})
export class NewA1PageComponent implements OnInit, AfterViewInit{

  constructor(
    private xonomyService: XonomyService, private a1Service: A1Service
  ) {}

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    let element = document.getElementById("editor");
    let specification = this.xonomyService
    let xmlString =
    Xonomy.render()
  }

  send() {

  }

}
