import {AfterViewInit, Component, OnInit} from '@angular/core';
import {XonomyService} from "../../service/xonomy.service";
import {P1Service} from "../../service/p1.service";
import xmlFormat from 'xml-formatter';

declare const Xonomy: any
@Component({
  selector: 'app-new-p1-page',
  templateUrl: './new-p1-page.component.html',
  styleUrls: ['./new-p1-page.component.css']
})
export class NewP1PageComponent implements OnInit, AfterViewInit {

  constructor(private xonomyService: XonomyService, private p1Service: P1Service) {
  }
  ngAfterViewInit(): void {
    let element = document.getElementById("editor");
    let specification = this.xonomyService.p1Specification
    let xmlString = "<z:zahtev_p1 xmlns:vc=\"http://www.w3.org/2007/XMLSchema-versioning\"" +
      " xmlns:slo=\"http://www.xml22.rs/slozeni/\"" +
      " xmlns:z=\"http://www.xml22.rs/zahtev-p1/\"" +
      " xmlns:pred=\"http://www.xml22.rs/zahtev-p1/predicate/\">" +
      "<z:sluzbeniDeo><z:brojPrijave></z:brojPrijave><z:datumPrijema>2000-01-01</z:datumPrijema><z:datumPodnosenja>2000-01-01</z:datumPodnosenja></z:sluzbeniDeo>" +
      "<z:nazivPronalaska><z:nazivSrpski></z:nazivSrpski><z:nazivEngleski></z:nazivEngleski></z:nazivPronalaska>" +
      "<z:podnosilac>" +
      "<slo:ime property=\"pred:ime\"></slo:ime>" +
      "<slo:prezime property=\"pred:prezime\"></slo:prezime>" +
      "<slo:adresa>" +
      "<slo:ulica></slo:ulica>" +
      "<slo:brojUlice></slo:brojUlice>" +
      "<slo:postanskiBroj></slo:postanskiBroj>" +
      "<slo:mesto></slo:mesto>" +
      "<slo:drzava></slo:drzava>" +
      "</slo:adresa>" +
      "<slo:email property=\"pred:email\"></slo:email>" +
      "<slo:telefon></slo:telefon>" +
      "</z:podnosilac>" +
      "<z:ovlascenje>" +
      "<z:punomocnikZastupanje>false</z:punomocnikZastupanje>" +
      "<z:podaci>" +
      "<slo:ime property=\"pred:ime\"></slo:ime>" +
      "<slo:prezime property=\"pred:prezime\"></slo:prezime>" +
      "<slo:adresa>" +
      "<slo:ulica></slo:ulica><slo:brojUlice></slo:brojUlice><slo:postanskiBroj></slo:postanskiBroj><slo:mesto></slo:mesto><slo:drzava></slo:drzava>" +
      "</slo:adresa>" +
      "<slo:email property=\"pred:email\"></slo:email>" +
      "<slo:telefon></slo:telefon>" +
      "</z:podaci>" +
      "</z:ovlascenje>" +
      "<z:tipPrijave><z:brojPrvobitnePrijave></z:brojPrvobitnePrijave><z:datumPodnosenja>2000-01-01</z:datumPodnosenja></z:tipPrijave>" +
      "</z:zahtev_p1>"
    Xonomy.render(xmlString, element, specification);
  }

  ngOnInit(): void {
  }

  send() {
    let text = Xonomy.harvest();
    let prettyXml = xmlFormat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + text);
    console.log(prettyXml);
    this.p1Service.createNewP1Xml(prettyXml).subscribe(result => console.log(result.status));
  }

}
