import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewP1PageComponent } from './new-p1-page.component';

describe('NewP1PageComponent', () => {
  let component: NewP1PageComponent;
  let fixture: ComponentFixture<NewP1PageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewP1PageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewP1PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
