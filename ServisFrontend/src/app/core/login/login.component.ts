import { Component,OnInit } from '@angular/core';
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {JwtHelperService} from "@auth0/angular-jwt";




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  form: FormGroup;

  constructor(
    private auth: AuthService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    })
  }

  ngOnInit(): void {
  }

  submit(): void {
    const auth: any = {};
    const jwt: JwtHelperService = new JwtHelperService();
    auth.username = this.form.value.username;
    auth.password = this.form.value.password;

    this.auth.login(auth).subscribe(
      result => {
        const token: any = result.headers.token;
        sessionStorage.setItem('user', token);
        // sessionStorage.setItem('expiresIn', token.expiresIn);
        const info = jwt.decodeToken(token);
        console.log(info);
        // if (info.role[0].name === "ROLE_ADMIN") {
        //   this.router.navigate(['/admin-homepage']);
        // } else if (info.role[0].name === "ROLE_MANAGER") {
        //   this.router.navigate(['/manager-homepage']);
      },
      error => {
        console.log(error);
        // this.toastr.error(error.error);
      }
    );
  }

}
